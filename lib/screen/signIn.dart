import 'dart:ui';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:mutual_events/api/login_api.dart';
import 'package:mutual_events/material/styling.dart';
import 'package:mutual_events/model/login_model.dart';
import 'package:mutual_events/screen/forgotPass.dart';
import 'package:mutual_events/screen/profile/profile.dart';
import 'package:mutual_events/screen/signUp.dart';
import 'package:mutual_events/widgets/String.dart';
import 'package:mutual_events/widgets/showdialogue.dart';
import 'package:mutual_events/widgets/terms.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

// ignore: must_be_immutable
class SignIn extends StatelessWidget {
  var _obsecureText = true.obs;
  final formKey = GlobalKey<FormState>();
  static const routeSignIn = '/signIn';

  LoginModel loginmodel = LoginModel();
  var progress = false.obs;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    // var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: AppTheme.background,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                        'assets/images/appIcon.png',
                        scale: 5,
                      ),
                      SizedBox(height: 10),
                      heading(width, context),
                      SizedBox(height: 45),
                      username(context),
                      Divider(),
                      password(),
                      SizedBox(height: 5),
                      forgotpassword(),
                      SizedBox(height: 30),
                      signInButton(),
                      SizedBox(height: 25),
                      Terms(), // terms of use and privacy policy
                      SizedBox(height: 10),
                      dontHaveAnAccount(),
                      SizedBox(height: 20),
                    ],
                  ),
                  Obx(
                    () => progress.value
                        ? CircularProgressIndicator()
                        : Container(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Row dontHaveAnAccount() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Dont have an account?',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 80),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Text(
              'Sign up',
              style: TextStyle(color: AppTheme.texttheme),
            ),
          ),
          onTap: () {
            Get.toNamed(SignUp.routeSignUp);
          },
        ),
      ],
    );
  }

  Widget signInButton() {
    return Row(
      children: [
        Expanded(
          child: SizedBox(
            height: 50,
            child: RaisedButton(
                color: AppTheme.theme,
                child: Text(
                  'Sign in',
                  style: TextStyle(color: Colors.white),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(13.0),
                ),
                onPressed: () {
                  //form.currentState.validate();
                  //form.currentState.save();
                  //popUp();
                  if (validateAndSave()) {
                    print(loginmodel.toJson());
                    progress.value = true;
                    LoginApi loginApi = LoginApi();
                    loginApi.login(loginmodel).then(
                      (value) async {
                        if (value.appData == null) {
                          final prefs = await SharedPreferences.getInstance();
                          prefs.setString('token', value.access);
                          progress.value = false;
                          Get.toNamed(Profile.routeProfile);
                        } else {
                          print(value.appData);
                          progress.value = false;
                          popUp(message: value.appData);
                        }
                      },
                    );
                  }
                }),
          ),
        ),
      ],
    );
  }

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  Text heading(double width, BuildContext context) {
    return Text(Strings.welcomeScreenTitle, style: AppTheme.welcomeTitle);
  }

  InkWell forgotpassword() {
    return InkWell(
      onTap: () => Get.toNamed(ForgetPass.routeForgotPass),
      child: Container(
        padding: EdgeInsets.all(20),
        child: Text(
          'Forgot Password?',
          style: TextStyle(color: AppTheme.texttheme),
        ),
      ),
    );
  }

  Obx password() {
    return Obx(
      () => Container(
        decoration: BoxDecoration(
            color: Color(0xfff5f4fa),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Row(
          children: [
            Flexible(
              child: TextFormField(
                  obscureText: _obsecureText.value,
                  onSaved: (val) => loginmodel.password = val,
                  //onFieldSubmitted: (value) => form.currentState.save(),
                  validator: (val) => val.length > 0 ? null : 'Enter Password',
                  decoration: buildInputDecoration("password", Icons.lock)),
            ),
            IconButton(
                icon: _obsecureText.value
                    ? Icon(
                        Icons.remove_red_eye_outlined,
                        color: Colors.grey,
                        size: 20,
                      )
                    : Icon(
                        Icons.no_encryption_outlined,
                        color: AppTheme.theme,
                        size: 20,
                      ),
                onPressed: () {
                  _obsecureText.value = !_obsecureText.value;
                })
          ],
        ),
      ),
    );
  }

  Container username(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Color(0xfff5f4fa),
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Row(
        children: [
          Flexible(
            child: TextFormField(
              textInputAction: TextInputAction.next,
              onSaved: (val) => loginmodel.username = val,
              //onFieldSubmitted: (value) => form.currentState.save(),
              validator: (val) => val.length > 0 ? null : 'Enter Username',
              decoration: buildInputDecoration("username", Icons.person_pin),
            ),
          ),
          //Icon(Icons.not_listed_location_sharp),
          IconButton(
              icon: Icon(
                Icons.not_listed_location_sharp,
                color: Colors.grey,
                size: 20,
              ),
              onPressed: () {
                popUpInfo(context);
              })
        ],
      ),
    );
  }

  InputDecoration buildInputDecoration(String text, IconData prefix) {
    return InputDecoration(
        labelText: text,
        prefixIcon: Icon(prefix),
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
        ),
        isDense: true);
  }

  Future popUpInfo(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: Colors.grey[800].withOpacity(0.9),
          title: Center(
            child: Text('Enter your Email address / \nUsername / Phone number',
                style: TextStyle(color: Colors.white, fontSize: 13)),
          ),
        );
      },
    );
  }
}
