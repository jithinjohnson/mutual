import 'package:flutter/material.dart';
import 'package:mutual_events/material/styling.dart';

class CatData {
  final String data;

  CatData(this.data);
}

List<CatData> catdata() {
  return <CatData>[
    CatData('Today\'s Event'),
    CatData('Entertainment'),
    CatData('Travel'),
    CatData('Sports'),
    CatData('Science'),
  ];
}

class CatShow extends StatelessWidget {
  final String text;

  const CatShow({Key key, this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: AppTheme.profilebox,
        child: Center(
            child: Text(
          text,
          style: TextStyle(fontWeight: FontWeight.w500),
        )),
      ),
    );
  }
}
