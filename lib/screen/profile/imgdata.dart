class ImgData {
  final String image;
  final String text;

  ImgData({this.image, this.text});
}

List<ImgData> imagedata() {
  return <ImgData>[
    ImgData(
      image: 'assets/images/img1.jpg',
      text: 'Holyday0',
    ),
    ImgData(
      image: 'assets/images/img2.jpg',
      text: 'Holyday1',
    ),
    ImgData(
      image: 'assets/images/img3.jpg',
      text: 'Holyday2',
    ),
    ImgData(
      image: 'assets/images/img4.jpg',
      text: 'Holyday3',
    ),
    ImgData(
      image: 'assets/images/img1.jpg',
      text: 'Holyday0',
    ),
    ImgData(
      image: 'assets/images/img2.jpg',
      text: 'Holyday1',
    ),
    ImgData(
      image: 'assets/images/img3.jpg',
      text: 'Holyday2',
    ),
    ImgData(
      image: 'assets/images/img4.jpg',
      text: 'Holyday3',
    ),
  ];
}
