import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mutual_events/material/styling.dart';
import 'package:mutual_events/screen/profile/imgdata.dart';
import 'package:mutual_events/screen/profile/imgshow.dart';
import 'package:mutual_events/screen/search/search.dart';
import 'package:mutual_events/ui/size_config.dart';
import 'package:mutual_events/widgets/String.dart';

// ignore: must_be_immutable
class Profile extends StatelessWidget {
  static const routeProfile = '/Profile';
  List<ImgData> imageData = imagedata();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Image.asset(
              'assets/images/backpro.jpeg',
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                  color: AppTheme.background,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              height: SizeConfig.heightMultiplier * 77,
            ),
          ),
          Positioned(
            width: SizeConfig.widthMultiplier * 100,
            top: 15 * SizeConfig.heightMultiplier,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    clipBehavior: Clip.antiAlias,
                    height: 16 * SizeConfig.heightMultiplier,
                    width: 15 * SizeConfig.heightMultiplier,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    child: Image.asset(
                      'assets/images/img1.jpg',
                      scale: 2,
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(Strings.profileTitle, style: AppTheme.profileTitle),
                      Icon(
                        Icons.domain_verification_rounded,
                        color: Colors.blue,
                      )
                    ],
                  ),
                  Text('@IamJithin', style: AppTheme.profileSubTitle),
                  Table(
                    children: [
                      TableRow(
                          //decoration: BoxDecoration(color: background),
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(children: [
                                Text('10M', style: AppTheme.profileNumbers),
                              ]),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(children: [
                                Text('5', style: AppTheme.profileNumbers)
                              ]),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(children: [
                                Text('26', style: AppTheme.profileNumbers)
                              ]),
                            ),
                          ]),
                      TableRow(
                          //decoration: BoxDecoration(color: Colors.white),
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(children: [
                                Text('Followers',
                                    style: AppTheme.profileSubTitle)
                              ]),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(children: [
                                Text('Following',
                                    style: AppTheme.profileSubTitle)
                              ]),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(children: [
                                Text('Events', style: AppTheme.profileSubTitle)
                              ]),
                            )
                          ]),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: 120,
                        height: 40,
                        decoration: AppTheme.profilebox,
                        child: Center(child: Text('Edit Profile')),
                      ),
                      Container(
                        width: 120,
                        height: 40,
                        decoration: AppTheme.profilebox,
                        child: Center(child: Text('Go live')),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Stories', style: AppTheme.profileTitle),
                        InkWell(
                            onTap: () {
                              _modalBottomSheetMenu(context);
                              //_bottomsheet(context);
                            },
                            child:
                                Text('See all', style: AppTheme.profileSeeAll))
                      ],
                    ),
                  ),
                  Container(
                    height: 180,
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: imageData.length,
                        itemBuilder: (BuildContext context, int item) {
                          return ImageShow(
                            image: imageData[item].image,
                            text: imageData[item].text,
                          );
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Media', style: AppTheme.profileTitle),
                        Text('See all', style: AppTheme.profileSeeAll)
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
      bottomSheet: BottomAppBar(
        child: Container(
          height: 50,
          decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: IconButton(
                  onPressed: () {
                    Get.toNamed(Search.routeSearch);
                    //print('hai');
                  },
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 20,
                  ),
                ),
              ),
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.menu,
                    color: Colors.white,
                    size: 20,
                  ),
                ),
              ),
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.calendar_today,
                    color: Colors.white,
                    size: 20,
                  ),
                ),
              ),
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.highlight_rounded,
                    color: Colors.white,
                    size: 20,
                  ),
                ),
              ),
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.person,
                    color: Colors.white,
                    size: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _modalBottomSheetMenu(BuildContext context) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (builder) {
          return DraggableScrollableSheet(
              initialChildSize: 0.4,
              minChildSize: 0.0,
              maxChildSize: 1.0,
              builder: (BuildContext context, myscrollController) {
                return Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(20.0),
                            topRight: const Radius.circular(20.0))),
                    child: SingleChildScrollView(
                      controller: myscrollController,
                      child: Column(
                        children: [
                          SizedBox(height: 10),
                          Container(
                            width: 58,
                            height: 7,
                            decoration: BoxDecoration(
                                color: Colors.grey[350],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                          ),
                          SizedBox(height: 10),
                          Text("Stories",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w500)),
                          SizedBox(height: 10),
                          Text(
                            "${imageData.length} items",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                                color: Colors.grey),
                          ),
                          SizedBox(height: 10),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: SizeConfig.widthMultiplier * 20),
                            child: GridView.builder(
                                //controller: myscrollController,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: imageData.length,
                                shrinkWrap: true,
                                gridDelegate:
                                    SliverGridDelegateWithMaxCrossAxisExtent(
                                  childAspectRatio: 0.7,
                                  maxCrossAxisExtent: 150,
                                  crossAxisSpacing: 0,
                                  mainAxisSpacing: 0.0,
                                ),
                                itemBuilder: (BuildContext context, int item) {
                                  return ImageShow(
                                    image: imageData[item].image,
                                    text: imageData[item].text,
                                  );
                                }),
                          )
                        ],
                      ),
                    ));
              });
        });
  }
}
