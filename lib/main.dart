import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mutual_events/screen/forgotPass.dart';
import 'package:mutual_events/screen/profile/profile.dart';
import 'package:mutual_events/screen/search/search.dart';
import 'package:mutual_events/screen/signIn.dart';
//import 'package:device_preview/device_preview.dart';
import 'package:mutual_events/ui/size_config.dart';

import 'screen/signUp.dart';

void main() {
  // runApp(
  //   DevicePreview(
  //     //enabled: !kReleaseMode,
  //     builder: (context) => MyApp(), // Wrap your app
  //   ),
  // );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);

            return GetMaterialApp(
              // locale: DevicePreview.locale(context),
              // builder: DevicePreview.appBuilder,
              title: 'Flutter Demo',
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                primarySwatch: Colors.deepOrange,
                canvasColor: Colors.transparent,
                visualDensity: VisualDensity.adaptivePlatformDensity,
              ),
              getPages: [
                GetPage(name: SignIn.routeSignIn, page: () => SignIn()),
                GetPage(
                    name: SignUp.routeSignUp,
                    page: () => SignUp(),
                    transition: Transition.leftToRight),
                GetPage(
                    name: ForgetPass.routeForgotPass, page: () => ForgetPass()),
                GetPage(name: Profile.routeProfile, page: () => Profile()),
                GetPage(name: Search.routeSearch, page: () => Search()),
              ],
              home: SignIn(),
            );
          },
        );
      },
    );
  }
}
