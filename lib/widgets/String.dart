class Strings {
  Strings._();

  static const String welcomeScreenTitle = '''Welcome To 
Mutual Event''';
  static const String profileTitle = 'Jithin K Johnson';

  static const String searchSubtitle = 'What can we help you to find, Daniel';
  static const String search2title = 'Recommended for you';
  static const String search2subtitle =
      'Experience events based on your interest';
}
