class ProfileModel {
  int id;
  List<Followers> followers;
  // List<Following> following;
  // List<Blocked> blocked;
  List<Events> events;
  //List<Media> media;
  List<Null> stories;
  bool emailVerified;
  String dob;
  Null countryCode;
  String phone;
  bool phoneVerified;
  String bio;
  String dp;
  String coverPhoto;
  bool publicStatus;
  String createdAt;
  String updatedAt;
  int user;
  String username;
  String email;
  String firstname;
  String surname;

  ProfileModel(
      {this.id,
      this.followers,
      //this.following,
      // this.blocked,
      this.events,
      //this.media,
      this.stories,
      this.emailVerified,
      this.dob,
      this.countryCode,
      this.phone,
      this.phoneVerified,
      this.bio,
      this.dp,
      this.coverPhoto,
      this.publicStatus,
      this.createdAt,
      this.updatedAt,
      this.user,
      this.username,
      this.email,
      this.firstname,
      this.surname});

  ProfileModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['followers'] != null) {
      followers = new List<Followers>();
      json['followers'].forEach((v) {
        followers.add(new Followers.fromJson(v));
      });
    }
    // if (json['following'] != null) {
    //   following = new List<Following>();
    //   json['following'].forEach((v) {
    //     following.add(new Following.fromJson(v));
    //   });
    // }
    // if (json['blocked'] != null) {
    //   blocked = new List<Blocked>();
    //   json['blocked'].forEach((v) {
    //     blocked.add(new Blocked.fromJson(v));
    //   });
    // }
    if (json['events'] != null) {
      events = new List<Events>();
      json['events'].forEach((v) {
        events.add(new Events.fromJson(v));
      });
    }
    // if (json['media'] != null) {
    //   media = new List<Media>();
    //   json['media'].forEach((v) {
    //     media.add(new Media.fromJson(v));
    //   });
    // }
    // if (json['stories'] != null) {
    //   stories = new List<Null>();
    //   json['stories'].forEach((v) {
    //     stories.add(new Null.fromJson(v));
    //   });
    // }
    emailVerified = json['email_verified'];
    dob = json['dob'];
    countryCode = json['country_code'];
    phone = json['phone'];
    phoneVerified = json['phone_verified'];
    bio = json['bio'];
    dp = json['dp'];
    coverPhoto = json['cover_photo'];
    publicStatus = json['public_status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'];
    username = json['username'];
    email = json['email'];
    firstname = json['firstname'];
    surname = json['surname'];
  }
}

class Followers {
  int id;
  String firstname;
  String surname;
  String username;
  String dp;

  Followers({this.id, this.firstname, this.surname, this.username, this.dp});

  Followers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstname = json['firstname'];
    surname = json['surname'];
    username = json['username'];
    dp = json['dp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstname'] = this.firstname;
    data['surname'] = this.surname;
    data['username'] = this.username;
    data['dp'] = this.dp;
    return data;
  }
}

class Events {
  int id;
  int profileId;
  String profileFirstname;
  String profileSurname;
  String profileUsername;
  String profileDp;
  List<EventImage> eventImage;
  String name;
  String description;
  String startDate;
  String endDate;
  String latitude;
  String longitude;
  bool reviewRating;
  List<Category> category;
  List<Review> review;
  int totalRating;
  //List<Participants> participants;

  Events({
    this.id,
    this.profileId,
    this.profileFirstname,
    this.profileSurname,
    this.profileUsername,
    this.profileDp,
    this.eventImage,
    this.name,
    this.description,
    this.startDate,
    this.endDate,
    this.latitude,
    this.longitude,
    this.reviewRating,
    this.category,
    this.review,
    this.totalRating,
    //this.participants,
  });

  Events.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    profileId = json['profile_id'];
    profileFirstname = json['profile_firstname'];
    profileSurname = json['profile_surname'];
    profileUsername = json['profile_username'];
    profileDp = json['profile_dp'];
    if (json['event_image'] != null) {
      eventImage = new List<EventImage>();
      json['event_image'].forEach((v) {
        eventImage.add(new EventImage.fromJson(v));
      });
    }
    name = json['name'];
    description = json['description'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    reviewRating = json['review_rating'];
    if (json['category'] != null) {
      category = new List<Category>();
      json['category'].forEach((v) {
        category.add(new Category.fromJson(v));
      });
    }
    if (json['review'] != null) {
      review = new List<Review>();
      json['review'].forEach((v) {
        review.add(new Review.fromJson(v));
      });
    }
    totalRating = json['total_rating'];
    // if (json['participants'] != null) {
    //   participants = new List<Participants>();
    //   json['participants'].forEach((v) {
    //     participants.add(new Participants.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['profile_id'] = this.profileId;
    data['profile_firstname'] = this.profileFirstname;
    data['profile_surname'] = this.profileSurname;
    data['profile_username'] = this.profileUsername;
    data['profile_dp'] = this.profileDp;
    if (this.eventImage != null) {
      data['event_image'] = this.eventImage.map((v) => v.toJson()).toList();
    }
    data['name'] = this.name;
    data['description'] = this.description;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['review_rating'] = this.reviewRating;
    if (this.category != null) {
      data['category'] = this.category.map((v) => v.toJson()).toList();
    }
    if (this.review != null) {
      data['review'] = this.review.map((v) => v.toJson()).toList();
    }
    data['total_rating'] = this.totalRating;
    // if (this.participants != null) {
    //   data['participants'] = this.participants.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class EventImage {
  int id;
  String image;

  EventImage({this.id, this.image});

  EventImage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    return data;
  }
}

class Category {
  int id;
  String name;
  String image;

  Category({this.id, this.name, this.image});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}

class Review {
  int id;
  int reviewerId;
  String reviewerFirstname;
  String reviewerSurname;
  String reviewerUsername;
  String reviewerDp;
  int rating;
  String review;

  Review(
      {this.id,
      this.reviewerId,
      this.reviewerFirstname,
      this.reviewerSurname,
      this.reviewerUsername,
      this.reviewerDp,
      this.rating,
      this.review});

  Review.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    reviewerId = json['reviewer_id'];
    reviewerFirstname = json['reviewer_firstname'];
    reviewerSurname = json['reviewer_surname'];
    reviewerUsername = json['reviewer_username'];
    reviewerDp = json['reviewer_dp'];
    rating = json['rating'];
    review = json['review'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['reviewer_id'] = this.reviewerId;
    data['reviewer_firstname'] = this.reviewerFirstname;
    data['reviewer_surname'] = this.reviewerSurname;
    data['reviewer_username'] = this.reviewerUsername;
    data['reviewer_dp'] = this.reviewerDp;
    data['rating'] = this.rating;
    data['review'] = this.review;
    return data;
  }
}
