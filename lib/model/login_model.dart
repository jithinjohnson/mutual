class LoginModel {
  String username;
  String password;

  LoginModel({this.username, this.password});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}

class LoginResponseModel {
  int id;
  String countryCode;
  String phone;
  String dob;
  String bio;
  String dp;
  String coverPhoto;
  bool emailVerified;
  String refresh;
  String access;
  String username;
  String email;
  String firstname;
  String surname;
  String appData;
  String devData;

  LoginResponseModel(
      {this.id,
      this.countryCode,
      this.phone,
      this.dob,
      this.bio,
      this.dp,
      this.coverPhoto,
      this.emailVerified,
      this.refresh,
      this.access,
      this.username,
      this.email,
      this.firstname,
      this.surname,
      this.appData,
      this.devData});

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) {
    return LoginResponseModel(
      id: json['id'],
      countryCode: json['country_code'],
      phone: json['phone'],
      dob: json['dob'],
      bio: json['bio'],
      dp: json['dp'],
      coverPhoto: json['cover_photo'],
      emailVerified: json['email_verified'],
      refresh: json['refresh'],
      access: json['access'],
      username: json['username'],
      email: json['email'],
      firstname: json['firstname'],
      surname: json['surname'],
      appData: json['app_data'] != Null ? json['app_data'] : '',
      devData: json['dev_data'] != Null ? json['dev_data'] : '',
    );
  }
}
