class SignupResponseModel {
  String appData;
  String devData;

  SignupResponseModel({this.appData, this.devData});
  factory SignupResponseModel.fromJson(Map<String, dynamic> json) {
    return SignupResponseModel(
        appData: json['app_data'] != null ? json['app_data'] : '',
        devData: json['dev_data'] != null ? json['dev_data'] : '');
  }

  // SignupResponseModel.fromJson(Map<String, dynamic> json) {
  //   appData = json['app_data'];
  //   devData = json['dev_data'];
  // }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['app_data'] = this.appData;
  //   data['dev_data'] = this.devData;
  //   return data;
  // }
}

class SignuModel {
  String firstname;
  String surname;
  String username;
  String email;
  String dob;
  String password;
  String countryCode;
  String phone;
  String otp;

  SignuModel({
    this.firstname,
    this.surname,
    this.username,
    this.email,
    this.dob,
    this.password,
    this.countryCode,
    this.phone,
    this.otp = "686868",
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstname'] = this.firstname;
    data['surname'] = this.surname;
    data['username'] = this.username;
    data['email'] = this.email;
    data['dob'] = this.dob;
    data['password'] = this.password;
    data['country_code'] = this.countryCode;
    data['phone'] = this.phone;
    data['otp'] = this.otp;

    return data;
  }
}
