import 'dart:convert' as convert;
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:mutual_events/model/profile_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginApi {
  Future<ProfileModel> login() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    var url = 'https://deep.mutualevents.co/api/v1/user/profile/';
    var response = await http.post(
      Uri.parse(url),
      headers: {HttpHeaders.authorizationHeader: stringValue},
    );
    return ProfileModel.fromJson(convert.jsonDecode(response.body));
  }
}
