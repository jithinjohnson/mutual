import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:mutual_events/model/login_model.dart';

class LoginApi {
  Future<LoginResponseModel> login(LoginModel signupmodel) async {
    String url = 'https://deep.mutualevents.co/api/v1/user/login/';
    var response = await http.post(
      Uri.parse(url),
      body: signupmodel.toJson(),
    );

    return LoginResponseModel.fromJson(convert.jsonDecode(response.body));
  }
}
