import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:mutual_events/model/signup_model.dart';

class SignUpApi {
  Future<SignupResponseModel> signup(SignuModel signupmodel) async {
    String url = 'https://deep.mutualevents.co/api/v1/user/signup/';
    var response = await http.post(Uri.parse(url), body: signupmodel.toJson());
    if (response.statusCode == 400) {
      return SignupResponseModel.fromJson(convert.jsonDecode(response.body));
    }
  }
}

class SignUpVerifyApi {
  Future signupverify(String number) async {
    String url = 'https://deep.mutualevents.co/api/v1/user/verify/phone/';
    var response = await http.post(Uri.parse(url), body: {"phone": number});
    if (response.statusCode == 200) {
      return response.statusCode;
    } else {
      return SignupResponseModel.fromJson(convert.jsonDecode(response.body));
    }
  }
}
